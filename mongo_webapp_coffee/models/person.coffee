###
  Simple DB model for representing
  a database collection. Using Mongoose.
###

mongoose = require 'mongoose'

PersonSchema = mongoose.Schema
  name: String,
  surname: String,
  age: Number


Person = mongoose.model 'Person', PersonSchema

module.exports = Person