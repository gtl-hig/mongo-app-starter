express = require 'express'
router = express.Router()

Person = require '../models/person'

cleanPeople = (people) ->
  people.forEach (person) ->
    person.__v = undefined;
    person._id = undefined;


#
# GET all the person listing.
#
router.get '/', (req, res) ->
  Person.find (err, people) ->
    if err
      res.send err
      return

    cleanPeople people
    res.json people


#
# Accept POST request with the json
# data representing a person
#
router.post '/new', (req, res) ->
    # adding new person to the DB
    p = new Person req.body
    p.save (err, person) ->
      if error
        res.send 'Problems when saving person to DB.'
        return

      res.send 'OK. New person added. Name: ', person.name



module.exports = router