express = require 'express'
router = express.Router()

Person = require '../models/person'

# GET home page, root /
router.get '/', (req, res) ->
  Person.find (err, people) ->
    if err
      res.send err
      return

    res.render 'index',
      title: 'Inside Mobile APP'
      people: people


module.exports = router
