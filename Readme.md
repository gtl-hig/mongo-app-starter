
# Web app example

## About

A simple skeleton project for JSON-enabled, MongoDB-based
store/fetch example of Web backend and HTTP API. 

There are two identical IntelliJ projects using exactly 
the same database:

   * JavaScript - mongo_webapp_js
   * CoffeeScript - mongo_webapp_coffee



## Main dependencies

   * IntelliJ (as the project IDE)
   * MongoDB - http://mongodb.org
   * Mongoose - http://mongoosejs.com/docs/index.html
   * NodeJS - http://nodejs.org

You should also install a Chrome extension, eg. 'Postman - REST Client',    
that allow testing of the HTTP API with JSON data from within the browser.




## Setup

To get the project opened in the IntelliJ, just say: Open Project in the main
IntelliJ dialog window that starts the IDE, and double click on the folder that
contains the appropriate project, i.e.:
	* mongo_webapp_js
	* mongo_webapp_coffee


In the main directory of each of the project, run:

	npm install

to get all the dependencies installed. The folder node_modules 
will be created and it will contain all the dependencies. 



## Command line execution

To run the project from command line, simply say:

	npm start

Or run the bin/server file with node, for JavaScript project,
or coffee, for CoffeeScript project:

	node ./bin/server
	coffee ./bin/server

