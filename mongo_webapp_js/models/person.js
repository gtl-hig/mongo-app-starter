/**
 * Created by mariusz on 13/11/14.
 */

var mongoose = require('mongoose');

var PersonSchema = mongoose.Schema({
    name: String,
    surname: String,
    age: Number
});

var Person = mongoose.model('Person', PersonSchema)

module.exports = Person;