var express = require('express');
var router = express.Router();

var Person = require('../models/person.js');

function cleanPeople(people) {
  people.forEach(function (person) {
      person.__v = undefined;
      person._id = undefined;
  });
};

/* GET all the person listing. */
router.get('/', function(req, res) {
  Person.find(function (err, people) {
      if (err) {
          res.send(err);
          return;
      }
      cleanPeople(people);
      res.json(people);
  });
});


/*
    Accept POST request with the json
    data representing a person
*/
router.post('/new', function(req, res) {
    // adding new person to the DB
    var p = new Person(req.body);
    p.save(function (err, saved_person) {
      if (err) {
        res.send('Problem saving person to database');
        return;
      }
      res.json('OK. Person added.');
    });    
});


module.exports = router;
