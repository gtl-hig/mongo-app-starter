var express = require('express');
var router = express.Router();

var Person = require('../models/person.js');

/* GET home page. */
router.get('/', function(req, res) {
    Person.find(function (err, people) {
        if (err) {
            res.send(err);
            return;
        }
        res.render('index', {
            title: 'Inside Mobile APP',
            people: people
        });
    });
});

module.exports = router;
